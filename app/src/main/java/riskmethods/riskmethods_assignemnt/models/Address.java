package riskmethods.riskmethods_assignemnt.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.JsonReader;
import android.util.JsonToken;

import java.io.IOException;

/**
 * Created by Mina on 01/09/15.
 */
public class Address implements Parcelable {
    private String address1;
    private String address2;
    private String zipCode;
    private String city;
    private String state;
    private String countryCode;

    public Address() {

    }

    public static Address createAddressfromJson(JsonReader reader) throws IOException {

        Address address = new Address();
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (reader.peek() != JsonToken.NULL) {
                if (name.equals("address1")) {
                    address.address1 = reader.nextString();
                } else if (name.equals("address2")) {
                    address.address2 = reader.nextString();
                } else if (name.equals("zipCode")) {
                    address.zipCode = reader.nextString();
                } else if (name.equals("city")) {
                    address.city = reader.nextString();
                } else if (name.equals("state")) {
                    address.state = reader.nextString();
                } else if (name.equals("countryCode")) {
                    address.countryCode = reader.nextString();
                } else {
                    reader.skipValue();
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return address;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    protected Address(Parcel in) {
        address1 = in.readString();
        address2 = in.readString();
        zipCode = in.readString();
        city = in.readString();
        state = in.readString();
        countryCode = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(address1);
        dest.writeString(address2);
        dest.writeString(zipCode);
        dest.writeString(city);
        dest.writeString(state);
        dest.writeString(countryCode);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Address> CREATOR = new Parcelable.Creator<Address>() {
        @Override
        public Address createFromParcel(Parcel in) {
            return new Address(in);
        }

        @Override
        public Address[] newArray(int size) {
            return new Address[size];
        }
    };
}
