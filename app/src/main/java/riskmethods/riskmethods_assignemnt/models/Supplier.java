package riskmethods.riskmethods_assignemnt.models;

import android.graphics.Color;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.JsonReader;
import android.util.JsonToken;
import android.util.Log;

import java.io.IOException;

/**
 * Created by Mina on 01/09/15.
 */
public class Supplier implements Parcelable {

    private String id;
    private String name;
    private String duns;
    private Address address;
    private ScoreCard scoreCard;
    private double score; // -1 represents no info, since 0 represents no risk


    public Supplier() {

    }

    public static Supplier createSupplierFromJson(JsonReader reader) throws IOException {

        Supplier supplier = new Supplier();
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (reader.peek() != JsonToken.NULL) {
                if (name.equals("id")) {
                    supplier.id = reader.nextString();
                } else if (name.equals("name")) {
                    supplier.name = reader.nextString();
                } else if (name.equals("duns")) {
                    supplier.duns = reader.nextString();
                } else if (name.equals("score")) {
                    supplier.score = reader.nextInt();
                } else if (name.equals("address")) {
                    supplier.address = Address.createAddressfromJson(reader);
                } else if (name.equals("score_card")) {
                    supplier.scoreCard = ScoreCard.createScoreCardFromJson(reader);
                    supplier.score = supplier.scoreCard.getRiskScore();
                } else {
                    reader.skipValue();
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();

        return supplier;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDuns() {
        return duns;
    }

    public void setDuns(String duns) {
        this.duns = duns;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public ScoreCard getScoreCard() {
        return scoreCard;
    }

    public void setScoreCard(ScoreCard scoreCard) {
        this.scoreCard = scoreCard;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public int getScoreColor() {
        if (score >= 0 && score < 20) {
            return Color.GREEN;
        } else if (score >= 20 && score < 40) {
            return Color.DKGRAY;
        } else if (score >= 40 && score < 60) {
            return Color.MAGENTA;
        } else if (score >= 60 && score < 80) {
            return Color.YELLOW;
        } else if (score >= 80) {
            return Color.RED;
        } else {
            return Color.BLACK;
        }

    }

    public String getScoreText() {
        if (score >= 0) {
            return score + "";
        } else {
            return "No Info";
        }
    }

    protected Supplier(Parcel in) {
        id = in.readString();
        name = in.readString();
        duns = in.readString();
        address = (Address) in.readValue(Address.class.getClassLoader());
        scoreCard = (ScoreCard) in.readValue(ScoreCard.class.getClassLoader());
        score = in.readDouble();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(duns);
        dest.writeValue(address);
        dest.writeValue(scoreCard);
        dest.writeDouble(score);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Supplier> CREATOR = new Parcelable.Creator<Supplier>() {
        @Override
        public Supplier createFromParcel(Parcel in) {
            return new Supplier(in);
        }

        @Override
        public Supplier[] newArray(int size) {
            return new Supplier[size];
        }
    };
}
