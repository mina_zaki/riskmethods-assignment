package riskmethods.riskmethods_assignemnt.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.JsonReader;
import android.util.JsonToken;

import java.io.IOException;
import java.net.URL;

/**
 * Created by Mina on 01/09/15.
 */
public class ScoreCard implements Parcelable {

    private String link;
    private double riskScore; // -1 is representing no information, 0 is no risk
    private double riskAmount;
    private double impactScore;
    private double impactVolume;
    private double impactAmount;


    public static ScoreCard createScoreCardFromJson(JsonReader reader) throws IOException {
        ScoreCard scoreCard = new ScoreCard();
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();

            // special case for risk score because null value for risk score is considered no information, different from 0 which is no risk
            if (name.equals("risk_score")) {
                if (reader.peek() != JsonToken.NULL) {
                    scoreCard.riskScore = reader.nextDouble();
                } else {
                    scoreCard.riskScore = -1;
                    reader.skipValue();
                }
            } else if (reader.peek() != JsonToken.NULL) {
                if (name.equals("risk_amount")) {
                    scoreCard.riskAmount = reader.nextDouble();
                } else if (name.equals("impact_score")) {
                    scoreCard.impactScore = reader.nextDouble();
                } else if (name.equals("impact_amount")) {
                    scoreCard.impactAmount = reader.nextDouble();
                } else if (name.equals("impact_volume")) {
                    scoreCard.impactVolume = reader.nextDouble();
                } else if (name.equals("link")) {
                    scoreCard.link = reader.nextString();
                } else {
                    reader.skipValue();
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return scoreCard;
    }

    public double getImpactWeight() {
        return impactWeight;
    }

    public void setImpactWeight(double impactWeight) {
        this.impactWeight = impactWeight;
    }

    public double getRiskAmount() {
        return riskAmount;
    }

    public void setRiskAmount(double riskAmount) {
        this.riskAmount = riskAmount;
    }

    public double getImpactAmount() {
        return impactAmount;
    }

    public void setImpactAmount(double impactAmount) {
        this.impactAmount = impactAmount;
    }

    private double impactWeight;

    public ScoreCard() {

    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public double getRiskScore() {
        return riskScore;
    }

    public void setRiskScore(double riskScore) {
        this.riskScore = riskScore;
    }

    public double getImpactScore() {
        return impactScore;
    }

    public void setImpactScore(double impactScore) {
        this.impactScore = impactScore;
    }

    public double getImpactVolume() {
        return impactVolume;
    }

    public void setImpactVolume(double impactVolume) {
        this.impactVolume = impactVolume;
    }

    protected ScoreCard(Parcel in) {
        link = in.readString();
        riskScore = in.readDouble();
        riskAmount = in.readDouble();
        impactScore = in.readDouble();
        impactVolume = in.readDouble();
        impactAmount = in.readDouble();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(link);
        dest.writeDouble(riskScore);
        dest.writeDouble(riskAmount);
        dest.writeDouble(impactScore);
        dest.writeDouble(impactVolume);
        dest.writeDouble(impactAmount);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<ScoreCard> CREATOR = new Parcelable.Creator<ScoreCard>() {
        @Override
        public ScoreCard createFromParcel(Parcel in) {
            return new ScoreCard(in);
        }

        @Override
        public ScoreCard[] newArray(int size) {
            return new ScoreCard[size];
        }
    };
}
