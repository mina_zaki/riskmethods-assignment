package riskmethods.riskmethods_assignemnt.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import riskmethods.riskmethods_assignemnt.R;
import riskmethods.riskmethods_assignemnt.models.Supplier;

/**
 * Created by Mina on 09/09/15.
 */
public class SupplierArrayAdapter extends ArrayAdapter<Supplier> {

    public SupplierArrayAdapter(Context context, List<Supplier> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Supplier supplier = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.supplier_list_row, parent, false);
        }
        TextView supplier_name = (TextView) convertView.findViewById(R.id.supplier_name);
        TextView duns = (TextView) convertView.findViewById(R.id.duns);
        TextView score = (TextView) convertView.findViewById(R.id.score);
        supplier_name.setText(supplier.getName());
        duns.setText(supplier.getDuns());
        score.setText(supplier.getScoreText());
        score.setTextColor(supplier.getScoreColor());
        return convertView;
    }
}
