package riskmethods.riskmethods_assignemnt.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

/**
 * Created by Mina on 10/09/15.
 */
public class ExceptionAlertDialog {

    public static void createExceptionAlertDialog(Context context){

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("We are sorry something went wrong, Please check your internet connectivity");
        builder.setCancelable(true);
        builder.setPositiveButton("OK!",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

}
