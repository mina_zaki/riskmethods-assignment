package riskmethods.riskmethods_assignemnt.activities;


import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import riskmethods.riskmethods_assignemnt.R;
import riskmethods.riskmethods_assignemnt.models.Supplier;

public class SupplierActivity extends AppCompatActivity {

    private Supplier supplier;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_supplier);
        this.supplier = getIntent().getParcelableExtra("supplier");
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(supplier.getName());
        setTextFields();
    }

    private void setTextFields() {
        //supplier
        ((TextView) this.findViewById(R.id.supplier_name)).setText(this.supplier.getName());
        ((TextView) this.findViewById(R.id.supplier_id)).setText(this.supplier.getId());
        ((TextView) this.findViewById(R.id.supplier_duns)).setText(this.supplier.getDuns());
        ((TextView) this.findViewById(R.id.supplier_score)).setText(this.supplier.getScoreText());
        ((TextView) this.findViewById(R.id.supplier_score)).setTextColor(this.supplier.getScoreColor());
        //score card
        ((TextView) this.findViewById(R.id.risk_score)).setText(this.supplier.getScoreText());
        ((TextView) this.findViewById(R.id.risk_score)).setTextColor(this.supplier.getScoreColor());
        ((TextView) this.findViewById(R.id.risk_amount)).setText(this.supplier.getScoreCard().getRiskAmount() + "");
        ((TextView) this.findViewById(R.id.impact_score)).setText(this.supplier.getScoreCard().getImpactScore() + "");
        ((TextView) this.findViewById(R.id.impact_amount)).setText(this.supplier.getScoreCard().getImpactAmount() + "");
        ((TextView) this.findViewById(R.id.impact_volume)).setText(this.supplier.getScoreCard().getImpactVolume() + "");
        ((TextView) this.findViewById(R.id.score_link)).setText(this.supplier.getScoreCard().getLink());
        //address
        ((TextView) this.findViewById(R.id.address1)).setText(this.supplier.getAddress().getAddress1());
        ((TextView) this.findViewById(R.id.address2)).setText(this.supplier.getAddress().getAddress2());
        ((TextView) this.findViewById(R.id.zip_code)).setText(this.supplier.getAddress().getZipCode());
        ((TextView) this.findViewById(R.id.state)).setText(this.supplier.getAddress().getState());
        ((TextView) this.findViewById(R.id.city)).setText(this.supplier.getAddress().getCity());
        ((TextView) this.findViewById(R.id.country_code)).setText(this.supplier.getAddress().getCountryCode());

    }
}
