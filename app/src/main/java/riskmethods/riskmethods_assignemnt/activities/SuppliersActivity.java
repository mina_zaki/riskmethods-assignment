package riskmethods.riskmethods_assignemnt.activities;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;

import riskmethods.riskmethods_assignemnt.R;
import riskmethods.riskmethods_assignemnt.models.Supplier;
import riskmethods.riskmethods_assignemnt.adapters.SupplierArrayAdapter;
import riskmethods.riskmethods_assignemnt.network_layer.ServiceHandler;
import riskmethods.riskmethods_assignemnt.utils.ExceptionAlertDialog;

public class SuppliersActivity extends ListActivity {

    private ArrayList<Supplier> suppliers;
    private SupplierArrayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suppliers);
        suppliers = new ArrayList<Supplier>();
        adapter = new SupplierArrayAdapter(this, suppliers);
        setListAdapter(adapter);
        new GetSuppliersTask().execute("");
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        Supplier supplier = suppliers.get(position);
        Intent intent = new Intent(this, SupplierActivity.class);
        intent.putExtra("supplier", supplier);
        startActivity(intent);
    }

    private class GetSuppliersTask extends AsyncTask<String, Integer, ArrayList<Supplier>> {


        private Exception exception;

        protected ArrayList<Supplier> doInBackground(String... urls) {
            ArrayList<Supplier> suppliers = null;
            try {
                suppliers = new ServiceHandler().getSuppliersList();
            } catch (Exception e) {
                this.exception = e;
            }
            return suppliers;
        }


        protected void onPostExecute(ArrayList<Supplier> suppliers) {
            if (exception == null) {
                SuppliersActivity.this.suppliers = suppliers;
                SuppliersActivity.this.adapter.addAll(suppliers);
            } else {
                Log.d("exception", exception.getLocalizedMessage());
                ExceptionAlertDialog.createExceptionAlertDialog(SuppliersActivity.this);
            }
        }
    }

}
