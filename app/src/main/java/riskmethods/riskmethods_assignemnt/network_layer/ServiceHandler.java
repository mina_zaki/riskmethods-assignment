package riskmethods.riskmethods_assignemnt.network_layer;

import android.util.Base64;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

import riskmethods.riskmethods_assignemnt.models.Supplier;

/**
 * Created by Mina on 01/09/15.
 */
public class ServiceHandler {

    private String urlString = "https://api.riskmethods.net/v1/suppliers.json";
    private String api_key = "61dbf36d800b2f054361b75169ae1e10";
    private InputStream is;

    public void getSuppliersRequest() throws IOException {
        URL url = new URL(this.urlString.toString());
        HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
        String authorizationString = "Basic " + Base64.encodeToString((api_key + ":" + api_key).getBytes(), Base64.NO_WRAP);
        conn.setRequestProperty("Authorization", authorizationString);
        conn.connect();
        this.is = conn.getInputStream();
    }


    public ArrayList<Supplier> getSuppliersList() throws IOException {
        getSuppliersRequest();
        JsonParser parser = new JsonParser();
        ArrayList<Supplier> suppliers = parser.readSupplierList(is);
        return suppliers;
    }
}
