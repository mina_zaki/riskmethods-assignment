package riskmethods.riskmethods_assignemnt.network_layer;

import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import riskmethods.riskmethods_assignemnt.models.Supplier;

/**
 * Created by Mina on 01/09/15.
 */
public class JsonParser {


    private JsonReader reader;


    public ArrayList<Supplier> readSupplierList(InputStream is) throws IOException {
        reader = new JsonReader(new InputStreamReader(is, "UTF-8"));
        ArrayList<Supplier> suppliers = new ArrayList<Supplier>();
        reader.beginObject(); // read suppliers object
        reader.skipValue(); // skip it since we dont need it
        reader.beginArray(); // begin the suppliers array
        while (reader.hasNext()) {
            suppliers.add(readSupplier());
        }
        reader.endArray(); // end suppliers array
        reader.close(); // close the reader
        return suppliers;
    }

    private Supplier readSupplier() throws IOException {
        return Supplier.createSupplierFromJson(reader);
    }
}
